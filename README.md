Hola WOLOX! Espero este código esté al alcance de sus expectativas, me habría encantado poder generar un código con una documentación tipo SWAGGER o algo por el estilo, pero mis tiempos acotados no me lo permitieron. De igual forma, aquí van las instrucciones de artefacto construido.

En primer lugar, en el código existen 3 directorios principales que permitieron desacoplar principalmente la lógica de rutas, con la lógica de negocio. La capa más externas es la de rutas, definida en el directorio "routes", ahí principalmente se realiza la orquestación que da solución al caso de uso solicitado. Cómo paso previo, se añadió Express-Validator para poder validar que los parámetros de los endpoint fueran en el formato deseado y con valores que tengan sentido para el negocio.

Por otro lado, la capa de servicios es la base de la lógica de negocio. Esta es la que almacena algunas operaciones a realizar o con la API externa o con la capa de persistencia. En este sentido, se intentó maximizar el uso de los Schemas de MongoDB para poder tener un mapeo entre las entidades y la lógica de negocio.

El último directorio clave es el de módulos, que principalmente cuenta con módulos que facilitan la implementación de la solución. En particular hay 2, uno relacionado a la instancia de la capa de persistencia, y el segundo está relacionado con el cliente HTTP que se utilizó para poder obtener los recursos desde la API externa.

PARA INICIAL EL APLICATIVO:

Lo principal es dar valores válidos a las variables ubicadas en el archivo ".env", para que puedan ser cargadas y reemplazadas al momento de la operación. Con las variables definidas, solo bastaría realizar "npm install" y posteriormente "npm start" para que inicie el aplicativo. Cabe destacar que al ejecutar este último comando, por consola aparecerá un mensaje que detallará el estado en el cuál se encuentra la API externa. En caso que se encuentre no disponible, el aplicativo finalizará, ya que no tiene sentido que un WRAPPER encienda cuando el recurso al cual se quiere acceder no se encuentra disponible.

Las variables descritas anteriormente son:

MONGO_URI=mongodb://localhost:27017 (URI de conexión de MONGODB)
MONGO_DB=coinGeckoWrapper (Nombre de la base de datos)
RESET_PASSWORD_KEY=wolox (Password utilizada para encriptación de JWT)
COIN_GECKO_API_URL=https://api.coingecko.com/api/v3 (URL de API externa).

Adjunto además una colección POSTMAN que hice para este proyecto, que contiene ejemplos de las request.