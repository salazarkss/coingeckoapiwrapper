const UserCryptoCurrencies = require('../models/UserCryptoCurrencies.js');
const CryptoCurrency = require('../models/CryptoCurrency.js');

class CryptoCurrencyService {
	async createUserCryptoCurrencies(userCryptoCurrenciesDTO) {
		try {
			const { userId } = userCryptoCurrenciesDTO;
			const userCryptoCurrencies = new UserCryptoCurrencies({ userId });
			await userCryptoCurrencies.save();
			return { code: 0, message: 'UserCryptoCurrencies created successfully' };
		}
		catch (e) {
			console.log(e);
			return { code: -1, message: 'Unable to create UserCryptoCurrencies' };
		}
	}

	async subscribeCryptoCurrencies(cryptoCurrenciesDTO) {
		try {
			const { cryptoCurrencies, userId } = cryptoCurrenciesDTO;
			const cryptoCurrenciesPromises = cryptoCurrencies.map(cryptoCurrency => {
				const newCryptoCurrencySchema = new CryptoCurrency(cryptoCurrency);
				return UserCryptoCurrencies.updateOne(
					{ userId, 'cryptoCurrencies.symbol': { $ne: cryptoCurrency.symbol } },
					{ $push: { cryptoCurrencies: newCryptoCurrencySchema } }
				);
			});
			await Promise.all(cryptoCurrenciesPromises);
			return { code: 0, message: 'Subscribe CryptoCurrencies successfully' };
		} catch(e) {
			console.log(e);
			return { code: -1, message: 'Unable to subscribe CryptoCurrencies' };
		}
	}
}

module.exports = CryptoCurrencyService;