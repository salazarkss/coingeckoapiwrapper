const User = require('../models/User.js');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const expiresInMinutes = 30 * 60;

class UserService {
	async create(userDTO) {
		try {
			const { name, lastname, username, password, preferredCurrency } = userDTO;
		  let user = new User({
		    name,
		    lastname,
		    username,
		    password: bcrypt.hashSync(password, 10),
		    preferredCurrency
		  });
		  const userSaveResponse = await user.save();
		  return { code: 0, message: userSaveResponse.id };
		}
		catch (e) {
			console.log("UserService create error");
			console.log(e);
			return { code: -1, message: 'User cannot be saved' };
		}
	}

	async login(username, password) {
		try {
			const selectedUser = await User.findOne({ username });
			if (!selectedUser) {
				return { code: -1, messsage: 'User or password is not correct' };
			}
			const passMatch = bcrypt.compareSync(password, selectedUser.password);
			if (!passMatch) {
				return { code: -1, messsage: 'User or password is not correct' };	
			}
			const token = jwt.sign({
				user: selectedUser
			}, process.env.RESET_PASSWORD_KEY, {
				expiresIn: expiresInMinutes
			});
			return {
				code: 0,
				message: 'User logged successfully',
				response: {
					token,
					username: selectedUser.username
				}
			};
		}
		catch (e) {
			console.log("UserService login error");
			console.log(e);
			return { code: -1, message: 'User cannot be logged' };
		}
	}

	async getId(username) {
		try {
			const selectedUser = await User.findOne({ username });
			if (!selectedUser) {
				return { code: -1, messsage: 'User do not exists' };
			}
			return { code: 0, message: selectedUser._id };
		}
		catch (e) {
			console.log(e);
			return { code: -1, message: 'Unable to get user currency' };
		}
	}

	async getCurrency(username) {
		try {
			const selectedUser = await User.findOne({ username });
			if (!selectedUser) {
				return { code: -1, messsage: 'User do not exists' };
			}
			return { code: 0, message: selectedUser.preferredCurrency };
		}
		catch (e) {
			console.log(e);
			return { code: -1, message: 'Unable to get user currency' };
		}
	}

	async validateJWT(token, username) {
		try {
			const tokenData = jwt.verify(token, process.env.RESET_PASSWORD_KEY);
			const { user } = tokenData;
			return { code: 0, message: 'Enable to validate user', isValid: user.username === username };
		}
		catch (e) {
			console.log("UserService validate error");
			console.log(e);
			return { code: -1, message: 'Unable to validate user', isValid: false };
		}
	}

}

module.exports = UserService;