const Request = require('../utils/request.js');

class CoinGeckoService {
	async isAPIEnabled() {
		try {
			const isEnableResponse = await Request(
				`${process.env.COIN_GECKO_API_URL}/ping`,
				'GET',
				{ 'accept': 'application/json' },
				null
			);
			if (isEnableResponse.status === 200) {
				return { code: 0, message: 'CoinGeckoAPI is enable!' };	
			}
			return { code: -1, message: 'CoinGeckoAPI is not enable!' };
		}
		catch (e) {
			console.log('Error => ', e);
			return { code: -1, message: 'CoinGeckoAPI is not enable!' };
		}
	}

	async getCryptoCurrenciesList() {
		try {
			const cryptoCurrenciesListResponse = await Request(
				`${process.env.COIN_GECKO_API_URL}/coins/list`,
				'GET',
				{ 'accept': 'application/json' },
				null
			);
			if (cryptoCurrenciesListResponse.status === 200) {
				const cryptoCurrenciesListAsJSON = await cryptoCurrenciesListResponse.json();
				return { code: 0, message: cryptoCurrenciesListAsJSON };
			}
			return { code: -1, message: 'Unable to fetch CryptoCurrencies List' };
		}
		catch (e) {
			console.log(e);
			return { code: -1, message: 'Unable to fetch CryptoCurrencies List' };
		}
	}

	async getCryptoCurrenciesByCurrency(dto) {
		try {
			const { currency, topN, order } = dto;
			const cryptoCurrenciesResponse = await Request(
				`${process.env.COIN_GECKO_API_URL}/coins/markets?vs_currency=${currency}`,
				'GET',
				{
					'Content-Type': 'application/json',
					'accept': 'application/json',
				},
				null
			);
			if (cryptoCurrenciesResponse.status === 200) {
				const cryptoCurrenciesListAsJSON = await cryptoCurrenciesResponse.json();
				const sortedCryptoCurrenciesList = cryptoCurrenciesListAsJSON.sort((cryptoCurrencyA, cryptoCurrencyB) => {
					if (order === 'ASC') {
						return cryptoCurrencyA.current_price - cryptoCurrencyB.current_price;
					}
					return cryptoCurrencyB.current_price - cryptoCurrencyA.current_price;
				});
				const filteredCryptoCurrenciesList = sortedCryptoCurrenciesList.slice(0, topN);
				return { code: 0, message: filteredCryptoCurrenciesList };
			}
			return { code: -1, message: 'Unable to fetch CryptoCurrencies List' };
		}
		catch (e) {
			console.log(e);
			return { code: -1, message: 'Unable to fetch CryptoCurrencies List' };
		}
	}

	async getAllCryptoCurrencies(dto) {
		try {
			const { currency } = dto;
			const cryptoCurrenciesResponse = await Request(
				`${process.env.COIN_GECKO_API_URL}/coins/markets?vs_currency=${currency}`,
				'GET',
				{
					'Content-Type': 'application/json',
					'accept': 'application/json',
				},
				null
			);
			if (cryptoCurrenciesResponse.status === 200) {
				const cryptoCurrenciesListAsJSON = await cryptoCurrenciesResponse.json();
				const formattedCryptoCurrenciesList = cryptoCurrenciesListAsJSON.map(cryptoCurrency => {
					return {
						symbol: cryptoCurrency.symbol,
						price: cryptoCurrency.current_price,
						name: cryptoCurrency.name,
						imageUrl: cryptoCurrency.image,
						lastUpdateAt: cryptoCurrency.last_updated,
					};
				});
				return { code: 0, message: formattedCryptoCurrenciesList };
			}
			return { code: -1, message: 'Unable to fetch CryptoCurrencies List' };
		}
		catch (e) {
			console.log(e);
			return { code: -1, message: 'Unable to fetch CryptoCurrencies List' };
		}
	}

}

module.exports = CoinGeckoService;