const mongoose = require('mongoose');

const mongoURL = process.env.MONGO_URI || 'mongodb://localhost:27017';
const mongoDB = process.env.MONGO_DB || 'test';

const connectionCallback = err => {
	if (err) {
		throw err;
	}
	console.log('MongoDB connected!');
};

const initConnection = () => {
	try {
		mongoose.connect(`${mongoURL}/${mongoDB}`,
		{
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useCreateIndex: true
		}, connectionCallback);
	}
	catch(e) {
		console.log(e);
		throw e;
	}
};

module.exports = {
	initConnection
};