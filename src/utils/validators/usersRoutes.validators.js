const { body } = require('express-validator');

const createUserValidators = [
	body('name').notEmpty().withMessage('name must be a valid string'),
	body('lastname').notEmpty().withMessage('lastname must be a valid string'),
	body('password').notEmpty().withMessage('password must be a valid string with at least length 8'),
	body('username').notEmpty().withMessage('username must be a valid string'),
	body('preferredCurrency').notEmpty().isIn(['ars', 'usd', 'eur']).withMessage('preferredCurrency must be one of this values: ars, usd, eur')
];

const loginUserValidators = [
	body('username').notEmpty().withMessage('username must be present'),
	body('password').notEmpty().withMessage('password must be present'),
];

module.exports = {
	createUserValidators,
	loginUserValidators,
};