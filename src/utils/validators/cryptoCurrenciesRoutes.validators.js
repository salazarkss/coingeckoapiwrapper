const { body } = require('express-validator');

const getUserTopCryptoCurrenciesValidators = [
	body('token').notEmpty().withMessage('name must be a valid string'),
	body('username').notEmpty().withMessage('username must be a valid string'),
	body('topN').isInt({ min: 0, max: 25 }).withMessage('topN must be a integer between 0 and 25'),
	body('order').isIn(['ASC', 'DESC']).withMessage('order must be a valid string with this values: ASC, DESC'),
];

const subscribeCryptoCurrenciesValidators = [
	body('username').notEmpty().withMessage('username must be a valid string'),
	body('token').notEmpty().withMessage('name must be a valid string'),
	body('cryptoCurrencies').isArray().notEmpty().withMessage('cryptoCurrencies must be a non empty array'),
	body('cryptoCurrencies.*.symbol').notEmpty().withMessage('cryptoCurrencies.symbol must be a valid string'),
	body('cryptoCurrencies.*.price').isInt({ min: 1 }).withMessage('cryptoCurrencies.price must be a valid integer > 0'),
	body('cryptoCurrencies.*.name').notEmpty().withMessage('cryptoCurrencies.name must be a valid string'),
	body('cryptoCurrencies.*.imageUrl').notEmpty().withMessage('cryptoCurrencies.imageUrl must be a valid string'),
	body('cryptoCurrencies.*.lastUpdateAt').isISO8601().withMessage('cryptoCurrencies.lastUpdateAt must be a valid date'),
];

const getAllCryptoCurrenciesValidators = [
	body('username').notEmpty().withMessage('username must be a valid string'),
];

module.exports = {
	getUserTopCryptoCurrenciesValidators,
	getAllCryptoCurrenciesValidators,
	subscribeCryptoCurrenciesValidators,
};