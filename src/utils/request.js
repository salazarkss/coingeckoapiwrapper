const fetch = require('node-fetch');

// This module is defined for not reimport node-fetch always we need to use it.
// Other importante thing is that with this structure we can change the async fetch library without problems.
module.exports = (url, method, headers, body) => fetch(
	url,
	{
		method,
		headers,
		body,
		timeout: 30000
	}
);