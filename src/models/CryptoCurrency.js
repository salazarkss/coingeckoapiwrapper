const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const { Schema } = mongoose;

const cryptoCurrencySchema = new Schema({
  id: {
    type: String,
    unique: true,
    required: [true, 'id is required'],
  },
  symbol: {
  	type: String,
  	required: [true, 'symbol is required'],
  },
  name: {
  	type: String,
  	required: [true, 'name is required'],
  },
  imageUrl: {
  	type: String,
  	required: [true, 'imageUrl is required'],
  },
  lastUpdateAt: {
  	type: Date,
  	default: Date.now
  },
});

cryptoCurrencySchema.plugin(uniqueValidator, {
	message: '{PATH} debe de ser único'
});

const cryptoCurrencyModel = mongoose.model('CryptoCurrency', cryptoCurrencySchema);

module.exports = cryptoCurrencyModel;
