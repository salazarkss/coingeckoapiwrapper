const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const CryptoCurrencySchema = require('./CryptoCurrency.js').schema;
const { Schema } = mongoose;

const userCryptoCurrenciesSchema = new Schema({
  userId: {
    type: String,
    unique: true,
    required: [true, 'userId is required'],
  },
  cryptoCurrencies: {
  	type: [CryptoCurrencySchema],
  	default: [],
  },
});

userCryptoCurrenciesSchema.plugin(uniqueValidator, {
	message: '{PATH} debe de ser único'
});

const userCryptoCurrenciesModel = mongoose.model('UserCryptoCurrencies', userCryptoCurrenciesSchema);

module.exports = userCryptoCurrenciesModel;
