const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const validPreferredCurrency = {
  values: ['eur', 'usd', 'ars'],
  message: '{VALUE} no es un moneda preferida válida'
};

const { Schema } = mongoose;

const hideUserPassword = doc => {
  const user = doc;
  const userObject = user.toObject();
  delete userObject.password;
  return userObject;
}

const userSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Name is required'],
  },
  lastname: {
    type: String,
    required: [true, 'Lastname is required'],
  },
  username: {
    type: String,
    unique: true,
    required: [true, "username is required"],
  },
  password: {
    type: String,
    required: [true, "Password is required"],
  },
  preferredCurrency: {
    type: String,
    required: [true, "Preferred currency is required"],
    enum: validPreferredCurrency,
  },
});

userSchema.set('toJSON', { transform: hideUserPassword });

userSchema.plugin(uniqueValidator, {
	message: '{PATH} debe de ser único'
});

const userModel = mongoose.model('User', userSchema);

module.exports = userModel;
