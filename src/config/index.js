const CoinGeckoServiceClass = require('../services/CoinGeckoService.js');
const CoinGeckoService = new CoinGeckoServiceClass();

// Ping CoinGeckoAPI if is enable. If isn't, doesn't make sense lauch WRAPPER
Promise.resolve(CoinGeckoService.isAPIEnabled()).then(response => {
	console.log(response.message);
	if (response.code !== 0) {
		process.exit();
	}
});

const PORT = 9000;

module.exports = {
	PORT
};