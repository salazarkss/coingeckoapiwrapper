const express = require('express');
const usersRouter = express.Router();
const UserServiceClass = require('../services/UserService.js');
const CryptoCurrencyServiceClass = require('../services/CryptoCurrencyService.js');
const UserService = new UserServiceClass();
const CryptoCurrencyService = new CryptoCurrencyServiceClass();
const { validationResult } = require('express-validator');
const {
	createUserValidators,
	loginUserValidators,
} = require('../utils/validators/usersRoutes.validators.js');

usersRouter.post('/create', createUserValidators, async (req, res) => {
	try {
	  const errors = validationResult(req);
	  if (!errors.isEmpty()) {
	    return res.status(400).json({ errors: errors.array() });
	  }
		const userDTO = req.body;
		const createUserResponse = await UserService.create(userDTO);
		if (createUserResponse.code !== 0) {
			res.statusCode = 500;
			res.end(JSON.stringify(createUserResponse));
		}
		const userCryptoCurrenciesDTO = { userId: createUserResponse.message };
		const createUserCryptoCurrenciesResponse = await CryptoCurrencyService.createUserCryptoCurrencies(userCryptoCurrenciesDTO);
		if (createUserCryptoCurrenciesResponse.code !== 0) {
			res.statusCode = 500;
			res.end(JSON.stringify(createUserCryptoCurrenciesResponse));
		}
		res.statusCode = 200;
		res.end(JSON.stringify(createUserCryptoCurrenciesResponse));
	}
	catch(e) {
		console.log(e);
		res.statusCode = 500;
		res.end(JSON.stringify({ code: -1, message: 'fatalError' }));
	}
});

usersRouter.post('/login', loginUserValidators, async (req, res) => {
	try {
	  const errors = validationResult(req);
	  if (!errors.isEmpty()) {
	    return res.status(400).json({ errors: errors.array() });
	  }
		const { username, password } = req.body;
		const loginResponse = await UserService.login(username, password);
		if (loginResponse.code === 0) {
			res.statusCode = 200;
		} else {
			res.statusCode = 500;
		}
		res.end(JSON.stringify(loginResponse));
	}
	catch (e) {
		console.log(e);
		res.statusCode = 500;
		res.end(JSON.stringify({ code: -1, message: 'fatalError' }));
	}
});

module.exports = usersRouter;