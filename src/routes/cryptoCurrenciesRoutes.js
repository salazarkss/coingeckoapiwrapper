const express = require('express');
const cryptoCurrenciesRouter = express.Router();
const CoinGeckoServiceClass = require('../services/CoinGeckoService.js');
const CoinGeckoService = new CoinGeckoServiceClass();
const UserServiceClass = require('../services/UserService.js');
const UserService = new UserServiceClass();
const CryptoCurrencyServiceClass = require('../services/CryptoCurrencyService.js');
const CryptoCurrencyService = new CryptoCurrencyServiceClass();
const { validationResult } = require('express-validator');
const {
	getUserTopCryptoCurrenciesValidators,
	getAllCryptoCurrenciesValidators,
	subscribeCryptoCurrenciesValidators,
} = require('../utils/validators/cryptoCurrenciesRoutes.validators.js');

cryptoCurrenciesRouter.post('/getUserTopCryptoCurrencies', getUserTopCryptoCurrenciesValidators, async (req, res) => {
	try {
	  const errors = validationResult(req);
	  if (!errors.isEmpty()) {
	    return res.status(400).json({ errors: errors.array() });
	  }
		const { token, username, topN, order } = req.body;
		const jwtValidationResponse = await UserService.validateJWT(token, username);
		if (jwtValidationResponse.code !== 0 || !jwtValidationResponse.isValid) {
			res.statusCode = 403;
			res.end(JSON.stringify(jwtValidationResponse));
			return;
		}
		const currencyResponse = await UserService.getCurrency(username);
		if (currencyResponse.code !== 0) {
			res.statusCode = 500;
			res.end(JSON.stringify(currencyResponse));
			return;
		}		
		const cryptoCurrenciesResponse = await CoinGeckoService.getCryptoCurrenciesByCurrency({
			currency: currencyResponse.message,
			topN,
			order,
		});
		if (cryptoCurrenciesResponse.code !== 0) {
			res.statusCode = 500;
			res.end(JSON.stringify(cryptoCurrenciesResponse));
			return;
		}
		res.statusCode = 200;
		res.end(JSON.stringify(cryptoCurrenciesResponse.message));
	}
	catch (e) {
		console.log(e);
		res.statusCode = 500;
		res.end('fatal Error');
	}
});

cryptoCurrenciesRouter.post('/subscribeCryptoCurrencies', subscribeCryptoCurrenciesValidators, async (req, res) => {
	try {
	  const errors = validationResult(req);
	  if (!errors.isEmpty()) {
	    return res.status(400).json({ errors: errors.array() });
	  }
		const { username, token, cryptoCurrencies } = req.body;
		const jwtValidationResponse = await UserService.validateJWT(token, username);
		if (jwtValidationResponse.code !== 0 || !jwtValidationResponse.isValid) {
			res.statusCode = 403;
			res.end(JSON.stringify(jwtValidationResponse));
			return;
		}
		const userIdResponse = await UserService.getId(username);
		if (userIdResponse.code !== 0) {
			res.statusCode = 500;
			res.end(JSON.stringify(userIdResponse));
			return;
		}
		const cryptoCurrenciesResponse = await CryptoCurrencyService.subscribeCryptoCurrencies({
			cryptoCurrencies,
			userId: userIdResponse.message,
		});
		if (cryptoCurrenciesResponse.code !== 0) {
			res.statusCode = 500;
			res.end(JSON.stringify(cryptoCurrenciesResponse));
			return;
		}
		res.statusCode = 200;
		res.end(JSON.stringify(cryptoCurrenciesResponse.message));
	}
	catch (e) {
		console.log(e);
		res.statusCode = 500;
		res.end('fatal Error');
	}
});

cryptoCurrenciesRouter.post('/getAllCryptoCurrencies', getAllCryptoCurrenciesValidators, async (req, res) => {
	try {
	  const errors = validationResult(req);
	  if (!errors.isEmpty()) {
	    return res.status(400).json({ errors: errors.array() });
	  }
		const { username } = req.body;
		const currencyResponse = await UserService.getCurrency(username);
		if (currencyResponse.code !== 0) {
			res.statusCode = 500;
			res.end(JSON.stringify(currencyResponse));
			return;
		}
		const cryptoCurrenciesResponse = await CoinGeckoService.getAllCryptoCurrencies({
			currency: currencyResponse.message,
		});
		if (cryptoCurrenciesResponse.code !== 0) {
			res.statusCode = 500;
			res.end(JSON.stringify(cryptoCurrenciesResponse));
			return;
		}
		res.statusCode = 200;
		res.end(JSON.stringify(cryptoCurrenciesResponse.message));
	}
	catch (e) {
		console.log(e);
		res.statusCode = 500;
		res.end('fatal Error');
	}
});

module.exports = cryptoCurrenciesRouter;