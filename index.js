require('dotenv').config();

const express = require('express');
const app = express();
const config = require('./src/config');
const cors = require('cors');

app.listen(config.PORT, () => {
	console.log(`Server Listening on port ${config.PORT}`)
});

// Modules imports
const mongoDB = require('./src/modules/MongoDB.js');

// Routes imports
const usersRoutes = require('./src/routes/usersRoutes.js');
const cryptoCurrenciesRoutes = require('./src/routes/cryptoCurrenciesRoutes.js');

// Database initialization
mongoDB.initConnection();

// Use JSON for Request Body's
app.use(express.json());
app.use(cors());
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

// Declare routes use
app.use('/users', usersRoutes);
app.use('/cryptoCurrencies', cryptoCurrenciesRoutes);
